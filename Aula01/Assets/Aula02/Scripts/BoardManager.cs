﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aula02
{

    public class BoardManager : MonoBehaviour
    {

        //Prefab
        [SerializeField]
        public GameObject[] tilePrefab;

        //Geramento Procedural
        [Header("Somente usada na Geração Procedural")]
        [Range(1,12)]
        public int linhas;
        [Range(1, 12)]
        public int colunas;

        //Private
        private List<GameObject> listaDeTiles = null;

        //Geramento Manual
        int[,] matrizMapa =
        {
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {2, 2, 2, 2},
            {0, 1, 2, 2},
        };

        // Start is called before the first frame update
        void Start()
        {
            //CriaTabuleiroManual();

            //CriaTabuleiroProcedural();
        }

        public void CriaTabuleiroManual()
        {
            InicializaTabuleiro();

            for (int l = 0; l < matrizMapa.GetLength(0); l++)
            {
                for (int c = 0; c < matrizMapa.GetLength(1); c++)
                {
                    Vector3 posicao = new Vector3
                    (
                        l - matrizMapa.GetLength(0) / 2,
                        0,
                        c - matrizMapa.GetLength(1) / 2
                    );

                    GameObject gO = Instantiate(tilePrefab[matrizMapa[l, c]], posicao, Quaternion.identity, this.transform);

                    listaDeTiles.Add(gO);
                }
            }

        }

        public void CriaTabuleiroProcedural()
        {
            InicializaTabuleiro();

            for(int l = 0; l < linhas; l++)
            {
                for(int c = 0; c < colunas; c++)
                {
                    Vector3 posicao = new Vector3
                    (
                        l - linhas / 2,
                        0,
                        c - colunas / 2
                    );

                    int rng = Random.Range(0, 3);

                    GameObject gO = Instantiate(tilePrefab[rng], posicao, Quaternion.identity, this.transform);

                    listaDeTiles.Add(gO);
                }
            }
        }

        public void InicializaTabuleiro()
        {
            if(listaDeTiles != null)
            {
                foreach(GameObject gO in listaDeTiles)
                {
                    Destroy(gO);
                }
            }

            listaDeTiles = new List<GameObject>();
        }

    }
}
