﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aula02
{
    public class MatrizExample : MonoBehaviour
    {

        private void Start()
        {
            // Declare a single-dimensional array. 
            int[] array1 = new int[5];

            // Declare and set array element values.
            int[] array2 = new int[] { 1, 3, 5, 7, 9 };

            // Alternative syntax.
            int[] array3 = { 1, 2, 3, 4, 5, 6 };

            // Declara uma array bi-dimensional
            int[,] multiDimensionalArray1 = new int[2, 3];

            // Declara uma array bi-dimensional e atribui os valores
            int[,] multiDimensionalArray2 = { { 1, 2, 3 }, { 4, 5, 6 } };

            // Declare a jagged array.
            int[][] jaggedArray = new int[6][];

            // Set the values of the first array in the jagged array structure.
            jaggedArray[0] = new int[4] { 1, 2, 3, 4 };

            //===============
            Debug.Log(multiDimensionalArray2[0, 0]);
            Debug.Log(multiDimensionalArray2[0, 1]);
            Debug.Log(multiDimensionalArray2[0, 2]);

            Debug.Log(multiDimensionalArray2[1, 0]);
            Debug.Log(multiDimensionalArray2[1, 1]);
            Debug.Log(multiDimensionalArray2[1, 2]);

            multiDimensionalArray1[0, 0] = 1;
            multiDimensionalArray1[0, 1] = 2;
            multiDimensionalArray1[0, 2] = 3;

            multiDimensionalArray1[1, 0] = 11;
            multiDimensionalArray1[1, 1] = 12;
            multiDimensionalArray1[1, 2] = 13;

        }





    }
}


