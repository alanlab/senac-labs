﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


namespace Aula02
{
    //Criação da Classe Inimigo para exemplo
    public class Inimigo
    {
        public int vida;
        public int ataque;
        public int defesa;

        //Construtor vazio do inimigo
        public Inimigo() { }

        //Construtor com os atributos
        public Inimigo(int _vida, int _ataque, int _defesa)
        {
            vida = _vida;
            ataque = _ataque;
            defesa = _defesa;
        }
    }


    public class VetorExample : MonoBehaviour
    {

        //Criação de um Vetor de Inteiro
        int[] numeros = new int[10];

        /*
        //Variaveis Inimigo
        public Inimigo inimigo1;
        public Inimigo inimigo2;
        public Inimigo inimigo3;
        public Inimigo inimigo4;
        public Inimigo inimigo5;
        public Inimigo inimigo6;
        public Inimigo inimigo7;

        //...

        public Inimigo inimigo99;
        public Inimigo inimigo100;
        */

        //Vetor de Inimigos
        public Inimigo[] inimigos = new Inimigo[100];


        private void Start()
        {
            //Inicializando sem Valores
            int[] numeros = new int[4];
            numeros[0] = 1;
            numeros[1] = 2;

            //Inicializando com Valores
            int[] numeros2 = { 0, 1, 2, 3, 4 };

            //Exemplos usando a classe Inimigo =========================================

            //Atribuindo um novo inimigo para o espaço '0' do Vetor de Inimigos
            inimigos[0] = new Inimigo();

            //Definindo os valores dos atributos do Inimigo no espaço '0'
            inimigos[0].vida = 10;
            inimigos[0].ataque = 5;
            inimigos[0].defesa = 3;

            //Usando um construtor
            inimigos[1] = new Inimigo(10, 5, 3);

            //Outros Exemplos =========================================

            //ExemploResizeArray();
            ExemploList();

        }

        //Exemplo de Resize de uma Array, feito manualmente
        void ExemploResizeArray()
        {
            Inimigo[] inimigosTemporario = new Inimigo[inimigos.Length + 100]; //Aqui o "inimigos.Length + 100" é o novo tamanho da Array
            inimigos.CopyTo(inimigosTemporario, 0);
            inimigos = inimigosTemporario;

            inimigos[102] = new Inimigo(30, 15, 23);

            foreach (Inimigo i in inimigos)
            {
                if (i != null)
                {
                    Debug.Log(i.vida);
                }
            }
        }

        //Exemplo do Uso de uma List<T> para criar uma lista de Inimigos
        void ExemploList()
        {
            List<Inimigo> listaInimigos = new List<Inimigo>();

            listaInimigos.Add(new Inimigo(10, 2, 5));
            listaInimigos.Add(new Inimigo(5, 1, 1));

            Debug.Log(listaInimigos[0].vida);
            Debug.Log(listaInimigos[1].vida);
        }

    }
}



