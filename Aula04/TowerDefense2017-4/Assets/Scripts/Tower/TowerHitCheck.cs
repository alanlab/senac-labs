﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerHitCheck : MonoBehaviour {

    public Tower tower = null;

    private void OnTriggerStay(Collider other)
    {
        //Se a torre estiver destruída ou não estiver pronto para atirar
        //Pula a checagem
        if (tower.dead || tower.stop || Time.time < tower.readyTime)
        {
            return;
        }

        //Se ele já tiver um alvo, verifica se o alvo está vivo
        if (tower.currentTarget != null)
        {
            //Se ainda está vivo, prepara o tiro e pula o resto.
            if (!tower.currentTarget.dead)
            {
                tower.PreparaTiro();
                return;
            }
        }
        //Caso contrário, procura por um novo alvo
        if (other.gameObject.tag == TagManager.EnemyTag)
        {
            Enemy newTarget = other.GetComponent<Enemy>();
            if (!newTarget.dead)
            {
                tower.currentTarget = newTarget;
                tower.PreparaTiro();
            }
            else
            {
                //Se até o novo alvo já estava morto, deixa nulo o alvo e ignora a checagem atual
                tower.currentTarget = null;
            }
        }
    }

    //Verifica se quem saiu é o Alvo atual
    private void OnTriggerExit(Collider other)
    {
        //Tá viva? Tem um alvo para verificar?
        if(!tower.dead && tower.currentTarget != null)
        {
            //Quem saiu é o nosso alvo?
            if (other.gameObject == tower.currentTarget.gameObject)
            {
                //Tira a mira do alvo, no próximo Stay vai procurar por um novo
                tower.currentTarget = null;
            }
        }
    }



}//END TowerHitCheck
