﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    [Header("Status")]
    public int vidaAtual = 20;
    public int vidaMax = 20;
    public int ataque = 3;
    public float respawnTime = 30f;
    public bool dead, stop = false;

    [Header("FireSettings")]
    public float fireSpeed = .5f;
    public float fireDelay = 1f;
    public Transform firePoint = null;
    public GameObject firePrefab = null;
    public Enemy currentTarget = null;
    public float readyTime = 0f;

    [Header("Damage Settings")]
    //Tempo de invencibilidade após receber um ataque, 
    //para não sofrer dois golpes consecutivos.
    public float invulnerableTime = .3f;
    public Material destroyedMaterial = null;
    public MeshRenderer[] crystalsMeshes = new MeshRenderer[5];

    [Header("Animator Reference")]
    public Animator anim = null;

    //Uso interno
    //Tempo de espera para poder receber outro ataque
    protected float damageCooldown = 0f;
    private Material respawnMaterial = null;

    public void PreparaTiro()
    {
        if (Time.time > readyTime)
        {
            readyTime = Time.time + fireDelay;
            Atira();
        }
    }

    private void Atira()
    {
        if(currentTarget != null)
        {
            //Instancia um novo projétil e atribui-o a uma referência
            TowerProjectile projectile = Instantiate(firePrefab,            //Objeto a ser Instanciado
                                                    firePoint.position,     //Posição onde será instanciado
                                                    Quaternion.identity,    //Rotação default
                                                    firePoint)              //Parent na Hierarquia da Cena
                                                    .GetComponent<TowerProjectile>();

            //Configura o Alvo do Tiro e seu dano
            projectile.SegueAlvo(ataque, currentTarget, currentTarget.hitPosition, fireSpeed);
        }
    }

    public void RecebeDano(int dano)
    {
        if (dead || stop)   //Se já estiver destruída, sai da função
        {
            return;
        }
 
        if (Time.time > damageCooldown)
        {
            damageCooldown = Time.time + invulnerableTime;
            vidaAtual -= dano;
            Debug.Log(name + " recebeu dano. Vida atual: " + vidaAtual);

            if (vidaAtual <= 0)
            {
                Morreu();
            }
        }
    }

    protected virtual void Morreu()
    {
        Debug.Log(name + " foi destruída.");
        dead = true;
        AnimacaoDestruida();
    }

    private void AnimacaoDestruida()
    {
        anim.SetBool("dead", true);
        int size = crystalsMeshes.Length;
        respawnMaterial = crystalsMeshes[0].material;
        for (int i = 0; i < size; i++)
        {
            Material[] materials = crystalsMeshes[i].materials;
            materials[0] = destroyedMaterial;
            crystalsMeshes[i].materials = materials;
        }
        //Para o Core não puxar o Respawn, 
        //o respawnTime é configurado como 0 nele
        if(respawnTime > 0f)
        {
            StartCoroutine(RespawnTimer());
        }
    }

    public void Para()
    {
        stop = true;
    }

    private IEnumerator RespawnTimer()
    {
        yield return new WaitForSeconds(respawnTime);
        Respawn();
        yield break;
    }

    private void Respawn()
    {
        dead = false;

        anim.SetBool("dead", false);
        vidaAtual = vidaMax;
        int size = crystalsMeshes.Length;
        for (int i = 0; i < size; i++)
        {
            Material[] materials = crystalsMeshes[i].materials;
            materials[0] = respawnMaterial;
            crystalsMeshes[i].materials = materials;
        }
    }

}
