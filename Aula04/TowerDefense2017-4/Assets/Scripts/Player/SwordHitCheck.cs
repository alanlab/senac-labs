﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
public class SwordHitCheck : MonoBehaviour {

    public Player stats;

    private void Start()
    {
        Collider _collider = GetComponent<Collider>();
        if (_collider.enabled)
        {
            _collider.enabled = false;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == TagManager.EnemyTag)
        {
            Enemy inimigo = other.GetComponent<Enemy>();
            if(inimigo != null && !inimigo.dead)
            {
                Debug.Log("Jogador acertou: " + other.gameObject);
                inimigo.RecebeDano(stats.ataque);
            }
        }
    }

}
