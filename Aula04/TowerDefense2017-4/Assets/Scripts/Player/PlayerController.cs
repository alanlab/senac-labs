﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Diz a Unity que para um objeto na cena usar esse script, ele precisa ter
//o Componente Character Controller
[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {

    //---- Variaveis públicas para manipularmos no Inspector
    //Movimento
    public float moveSpeed = 5f;    //Velocidade de Movimento
    public float rotateSpeed = 12f; //Velocidade de Rotação
    public float inputCheck = .1f;  //Valor de Verificação de Input

    //Ataque
    public float attackCooldown = .3f;

    //---- Referencias aos Componentes do Visual do Jogador
    public Transform playerVisual = null;   //Referência ao Visual do Jogador
    public Animator playerAnimator = null;       //Referência ao Animator do Jogador

    //---- Referencia aos componentes do Controlador
    private CharacterController controller = null;

    //Referência aos Stats
    private Player player;

    //---- Variaveis privadas para uso local
    private float moveHorizontal, moveVertical = 0f;

    private bool attack = false;
    private float readyTime = 0f;

    private Vector3 moveDirection = Vector3.zero;
    private Quaternion desiredRotation = Quaternion.identity;

    // Inicializamos a referência no Awake e inicializamos a rotação com a atual do Player
    private void Awake ()
    {
        controller = GetComponent<CharacterController>();
        player = GetComponent<Player>();
        desiredRotation = playerVisual.rotation;
    }

    //Executa a cada quadro
    private void Update()
    {
        if(player.dead || player.stop)
        {
            return;
        }

        RecebeInput();

        CalculaDirecao();

        Rotaciona();

        Movimenta();

        VerificaAtaque();
    }

    private void RecebeInput()
    {
        //Captura o Input das Axis Horizontal e Vertical e armazena nas variáveis
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");

        //Captura Input do botão Fire 1 - Botão Esquerdo do Mouse ou Tecla CTRL do lado esquerdo
        attack = Input.GetButtonDown("Fire1");
    }

    private void CalculaDirecao()
    {
        //Com o Input capturado, vamos calcular nossa direção
        Vector3 direcao = new Vector3(moveHorizontal, 0, moveVertical);
        //Normaliza a direção para não termos um movimento mais rápido ao andar em diagonal
        direcao = direcao.normalized;

        moveDirection = direcao;
    }

    private void Rotaciona()
    {
        //Checa se realmente houve um input intencional, caso sim, atualiza a rotação para a direção que o Player irá
        if (Mathf.Abs(moveHorizontal) >= inputCheck || Mathf.Abs(moveVertical) >= inputCheck)
        {
            desiredRotation = Quaternion.LookRotation(moveDirection);
        }
        //Rotaciona o Player suavemente usando a função de Interpolação (Lerp)
        playerVisual.rotation = Quaternion.Lerp(playerVisual.rotation, desiredRotation, rotateSpeed * Time.deltaTime);
    }

    private void Movimenta()
    {
        //Atribuímos o movimento com base na Direção Calculada * Velocidade * deltaTime 
        //(Diferença de tempo com base no último update)
        Vector3 movement = moveDirection * moveSpeed * Time.deltaTime;

        //Como não estamos mais utilizando um Rigidbody, aplicamos a gravidade manualmente
        movement.y = Physics.gravity.y;

        //Com o movimento calculado, usamos a função do Character Controller para aplciar isso ao Jogador
        controller.Move(movement);

        AnimacaoMovimento();
    }

    private void AnimacaoMovimento()
    {
        bool running = controller.velocity.magnitude > 0f;
        playerAnimator.SetBool("running", running);
    }

    public void VerificaAtaque()
    {
        if (attack && Time.time > readyTime)
        {
            readyTime = Time.time + attackCooldown;
            Ataca();
        }
    }

    public void Ataca()
    {
        playerAnimator.SetTrigger("attack");
        Debug.Log("Atacou!");
    }


}
