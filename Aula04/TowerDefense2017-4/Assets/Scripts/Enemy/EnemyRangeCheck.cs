﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeCheck : MonoBehaviour {

    public Enemy enemy;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == enemy.currentTarget)
        {
            Debug.Log("Alvo próximo!");
            enemy.targetInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == enemy.currentTarget)
        {
            enemy.targetInRange = false;
        }
    }

}
