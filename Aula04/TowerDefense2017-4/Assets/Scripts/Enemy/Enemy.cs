﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum TargetPriority
{
    tower,
    player,
    core
}

public class Enemy : MonoBehaviour {

    [Header("Status")]
    public int vidaAtual = 30;
    public int vidaMax = 30;
    public int ataque = 2;
    public bool dead = false;
    public bool stop = false;

    [Header("Movement Settings")]
    public NavMeshAgent agent = null;

    [Header("Target Settings")]
    public TargetPriority targetingType = TargetPriority.tower;
    public GameObject currentTarget = null;

    [Header("Attack Settings")]
    public bool targetInRange = false;
    public float attackDelay = .5f;

    [Header("Damage Settings")]
    public Transform hitPosition = null;
    //Tempo de invencibilidade após receber um ataque, 
    //para não sofrer dois golpes consecutivos.
    public float invulnerableTime = .3f;    

    [Header("Animator Reference")]
    public Animator anim;

    //Uso interno
    private float readyTime = 0f;
    private float damageCooldown = 0f;

    //Targeting References
    private Tower towerTarget = null;
    private Player playerTarget = null;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if(dead || stop)
        {
            return;
        }
        if (Mathf.Abs(agent.velocity.x) > 0 || Mathf.Abs(agent.velocity.z) > 0)
        {
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }

        VerificaAlvo();

        if (currentTarget != null)
        {
            SegueAlvo();
        }
    }

    private void VerificaAlvo()
    {
        if (currentTarget == null)
        {
            ProcuraAlvo();
        }
        else
        {
            if(targetingType == TargetPriority.tower)
            {
                if (towerTarget.dead)
                {
                    targetInRange = false;
                    currentTarget = null;
                    ProcuraAlvo();
                }
            }
        }
    }

    private void ProcuraAlvo()
    {
        switch (targetingType)
        {
            case TargetPriority.tower:
                Tower t = GameManager.instance.RecebeTorreMaisProxima(transform.position);
                if (t != null)
                {
                    towerTarget = t;
                    currentTarget = towerTarget.gameObject;
                }
                else
                {
                    int rng = Random.Range(0, 2);
                    if(rng > 1)
                    {
                        targetingType = TargetPriority.player;
                    }
                    else
                    {
                        targetingType = TargetPriority.core;
                    }
                }
                break;
            case TargetPriority.player:
                playerTarget = GameManager.instance.player;
                currentTarget = playerTarget.gameObject;
                break;
            case TargetPriority.core:
                towerTarget = GameManager.instance.core;
                currentTarget = towerTarget.gameObject;
                break;
        }
    }

    private void SegueAlvo()
    {
        if(!targetInRange)
        {
            agent.SetDestination(currentTarget.transform.position);
        }
        else
        {
            PreparaAtaque();
        }
    }

    private void PreparaAtaque()
    {
        if (Time.time > readyTime)
        {
            readyTime = Time.time + attackDelay;
            Ataca();
        }
    }

    private void Ataca()
    {
        transform.LookAt(currentTarget.transform.position);
        switch (targetingType)
        {
            case TargetPriority.tower:
                if(!towerTarget.dead)
                {
                    anim.SetTrigger("attack");
                    towerTarget.RecebeDano(ataque);
                }
                else
                {
                    targetInRange = false;
                    currentTarget = null;
                }
                break;
            case TargetPriority.player:
                if (!playerTarget.dead)
                {
                    anim.SetTrigger("attack");
                    playerTarget.RecebeDano(ataque);                  
                }
                break;
            case TargetPriority.core:
                if (!towerTarget.dead)
                {
                    anim.SetTrigger("attack");
                    towerTarget.RecebeDano(ataque);
                }
                break;
        }
    }

    public void RecebeDano(int dano)
    {
        if(Time.time > damageCooldown)
        {
            damageCooldown = Time.time + invulnerableTime;

            anim.SetTrigger("hit");
            vidaAtual -= dano;

            Debug.Log(name + " recebeu dano. Vida atual: " + vidaAtual);

            if (vidaAtual <= 0)
            {
                Morreu();               
            }
        }
    }

    private void Morreu()
    {
        Debug.Log(name + " foi morto.");
        GameManager.instance.em.RemoveInimigo(this);
        dead = true;
        agent.isStopped = true;
        targetInRange = false;
        anim.SetBool("dead", true);
        GetComponent<Collider>().enabled = false;
        Destroy(gameObject, 3f);
    }

    public void Para()
    {
        stop = true;
        agent.isStopped = true;
    }

}
