﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public GameObject panelGameOver = null;

    public void ChamaTelaGameOver()
    {
        if(panelGameOver != null)
        {
            panelGameOver.SetActive(true);
        }
    }

}
